declare module 'kafer-module' {
    export interface User {
        id: number;
        email: string;
        password: string;
    }

    export interface Company {
        id: number;
        name: string;
        warehouses: Warehouse[];
    }

    export interface Warehouse {
        id: number;
        name: string;
        places: WarehousePlace[];
    }

    export interface WarehousePlace {
        id: number;
        name: string;
    }

    export interface Product {
        id: number;
        barCode: string;
        name: string;
        price: number;
        quantity: number;
        warehouse: Warehouse | number;
        warehousePlace: WarehousePlace | number;
    }
}
